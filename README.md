Translator 

Project that translates from Morse code to Natural Human Languaje and vice versa.

API end-points

 * /translate/2text
 
 * /translate/2morse

 * /translate/bits2morse


Usage example :

POST Request to https://translator-morse-api.herokuapp.com/translate/2morse

Content-Type : application/json
Body: 
{
    "text": "This is a translation proof."
}