
const { translate2morse } = require('../services/translateToMorse');
const { translate2human } = require('../services/translateToText');
const { decodeBits2Morse } = require('../services/translateFromBits');

class Controller {
    
    Controller() {}

    async toText (req, res) {   
        let success;
        const message = req.body.text;
        
        try {      
            success = await translate2human(message);                     
            if (success) {
                res.status(200).send({ code: 200, response: success } );
            } else {
                res.status(204).send({ code: 204, response: success });
            }
        } catch (error) {
            res.status(500).send(error);
        }
    }

    async toMorse (req, res) {   
        let success;
        const message = req.body.text;        
                
        try {      
            success = await translate2morse(message);
            if (success) {                
                res.status(200).json({ code: 200, response: success});
            } else {
                res.status(204).send({ status: 204, response: success });
            }
        } catch (error) {
            res.status(500).send(error);
        }
    }

    async decodeFromBits (req, res) {   
        let success;
        const message = req.body.text;        
                
        try {      
            success = await decodeBits2Morse(message);

            if (success) {
                res.status(200).json({ code: 200, response: success});
            } else {
                res.status(204).send({ status: 204, response: success });
            }
        } catch (error) {
            res.status(500).send(error);
        }
    }
}

module.exports = Controller;