const express = require('express');
const router = require('../routers/routes')
const bodyParser = require('body-parser');

const app = new express();

const welcomePage = `<h1>Morse Translator</h1><h2>API end-points</h2>* /translate/2text <br>* /translah3te/2morse <br> * /translate/bits2morse <br> <h4>Daniel Rey</h4>`;

app.use(bodyParser.json());
app.use(router);

app.use('/', (req, res) => {
    res.status(200).send(welcomePage);
});

app.use((req, res, next)=> { 
    res.status(404).send('Page not found.');
    next();
});


app.use((req, res, next)=> {
    console.log(req.method, req.url, res.statusCode);
    next();
});

module.exports = app;