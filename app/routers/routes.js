const express = require('express');
const Controller =  require('../controller/controller');

const router = express.Router();
const controller = new Controller();

router.post('/translate/2text', controller.toText);
router.post('/translate/2morse', controller.toMorse);
router.post('/translate/bits2morse', controller.decodeFromBits);

module.exports = router;