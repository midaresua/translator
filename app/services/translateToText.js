const { morseLanguaje, humanLanguaje }  = require('../models/languajes');

const translate2human = async (morse) => {
    const humanMessage = [];
    const finalMessage =[];

    const trimed = morse.replace(/\s\s+/g, '*').trim().split('*');   

    trimed.forEach(el => {
        humanMessage.push(...el.split(' '));
        humanMessage.push('*');
    });
    
    humanMessage.forEach(element => {
        const position = morseLanguaje.indexOf(element);
        
        if(position >= 0) {
            finalMessage.push(humanLanguaje[position]);
        } else if (element === '*') {
            finalMessage.push(' ');
        }
        
    });

    return finalMessage.join('').trim();
};

module.exports = {
    translate2human
};
