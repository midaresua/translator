const { morseLanguaje, humanLanguaje } = require('../models/languajes');

const translate2morse = async (naturalLanguaje) => {
    const morseMessage = [];
    const finalMessage = [];
    
    const trimed = naturalLanguaje.toUpperCase().replace(/\s+/g, '*').trim().split('*');   

    trimed.forEach(el => {
        morseMessage.push(...el.split(''));
        morseMessage.push('*');
    });

    morseMessage.forEach(element => {
        const position = humanLanguaje.indexOf(element);

        if(position >= 0) {
            finalMessage.push(morseLanguaje[position]);
            finalMessage.push(' ');
        } else if (element === '*') {
            finalMessage.push(' ');
        }

    });
    
    return finalMessage.join('').trim();
};

module.exports = {
    translate2morse
};