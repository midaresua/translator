const decodeBits2Morse = async (bits) => {
        const messageArray = [];
        const morseMessage = [];

        const spaces = bits.split('1').filter(n => n);
        const points = bits.split('0').filter(n => n);
        
        const spaceAvg = getBinaryAverage(spaces);
        const pointAvg = getBinaryAverage(points);
        
        for (let index = 0; index < spaces.length; index++) {
            messageArray.push(spaces[index]);
            if (points[index]) {
                messageArray.push(points[index]);
            }
        }
    
        messageArray.forEach(element => {
            if (element.includes('0')) {
                if (element.length <= spaceAvg) {
                    morseMessage.push(' ');
                } else {
                    morseMessage.push('*');
                }
            } else {
                if (element.length <= pointAvg) {
                    morseMessage.push('.');
                } else {
                    morseMessage.push('-');
                }
            }
        });

       return morseMessage.join('').replace(/\s+/g, '').replace(/\*+/g, ' ').trim();
    };
    
    const getBinaryAverage = (spaces) => {
        const numbers = spaces.map(n => n.length);
        const sum = numbers.reduce((a, b) => a + b, 0);
            
        return Math.floor(sum / numbers.length);
    };
    

module.exports = {
    decodeBits2Morse
};